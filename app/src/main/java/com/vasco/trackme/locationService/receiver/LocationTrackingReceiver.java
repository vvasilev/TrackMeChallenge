package com.vasco.trackme.locationService.receiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vasco.trackme.ui.utils.Const;

public class LocationTrackingReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (context instanceof LocationTrackingReceiverInterface){
            sendAction((LocationTrackingReceiverInterface) context, intent);
        }
    }

    private void sendAction(LocationTrackingReceiverInterface listener, Intent intent){
        if (intent.getExtras().getString(Const.ACTION).equals(Const.UPDATE)) {
            listener.onUpdateLocation();
        }else if (intent.getExtras().getString(Const.ACTION).equals(Const.REMOVE)){
            listener.onRemoveLocation();
        }
    }
}
