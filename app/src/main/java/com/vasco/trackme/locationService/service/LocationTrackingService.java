package com.vasco.trackme.locationService.service;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.vasco.trackme.activities.MainActivity;
import com.vasco.trackme.Preferences;
import com.vasco.trackme.R;
import com.vasco.trackme.TheApplication;
import com.vasco.trackme.db.DBHelper;
import com.vasco.trackme.db.dao.CoordinatesDao;
import com.vasco.trackme.db.dao.TripDao;
import com.vasco.trackme.model.CoordinatesData;
import com.vasco.trackme.model.TripData;
import com.vasco.trackme.ui.utils.Const;
import com.vasco.trackme.ui.utils.NotificationUtils;

import java.util.List;

public class LocationTrackingService extends Service {

    private static final String MINIMUM_POINTS_PATH = "3";

    private static final int LOCATION_INTERVAL = 1000; // updates each second
    private static final float LOCATION_DISTANCE = 3f; // update every 3 meters

    private boolean isFirstPositionStored;

    private TheApplication application;
    private LocationManager locationManager;
    private Preferences preferences;
    private NotificationUtils notficationOreo;
    private DBHelper dbHelper;
    private TripDao journeyDetailDao;
    private TripData tripData;
    private CoordinatesDao coordinatesDetailDao;

    LocationListener[] locationListeners = new LocationListener[]{
            new LocationListener()
    };

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        application = (TheApplication) getApplication();
        preferences = application.getPreferences();
        dbHelper = application.getDBHelper();
        initializeLocationManager(application);
        createDao(dbHelper);
        if (!preferences.isTrackingLocationService()) {
            createTripAndSave(preferences);
            preferences.setTrackingLocationService(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                showNotificationOnOreoDevices();
            } else {
                showNotification();
            }
        } else {
            tripData = journeyDetailDao.findById(preferences.getTripId());
        }

        locationManager.requestLocationUpdates(
                LocationManager.PASSIVE_PROVIDER,
                LOCATION_INTERVAL,
                LOCATION_DISTANCE,
                locationListeners[0]
        );
    }

    private void createDao(DBHelper databaseHelper) {
        coordinatesDetailDao = databaseHelper.getCoordinatesDetailDao();
        journeyDetailDao = databaseHelper.getTripDataDao();
    }

    private void createTripAndSave(Preferences appPreferences) {
        tripData = new TripData(String.valueOf(System.currentTimeMillis()));
        journeyDetailDao.insert(tripData, TripDao.TABLE_NAME);
        appPreferences.setTripId(tripData.getDBid());
    }


    private void checkSizePathPoints() {
        String selection = CoordinatesData.Fields.TRIP_ID + "=?";
        String[] selectionArgs = new String[]{String.valueOf(tripData.getDBid())};
        List<CoordinatesData> roadPoint = coordinatesDetailDao.findMany(selection, selectionArgs, MINIMUM_POINTS_PATH);
        if (roadPoint != null && roadPoint.size() < 2) {
            journeyDetailDao.delete(tripData);
            sendMessage(Const.REMOVE);
            Toast.makeText(application, application.getResources().getString(R.string.pathWasShort), Toast.LENGTH_SHORT).show();
        }
    }

    private void cancelNotification() {
        NotificationManager notificationManager = (NotificationManager) application.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(Const.TRACK_LOCATION);
    }

    private void initializeLocationManager(TheApplication application) {
        if (locationManager == null) {
            locationManager = (LocationManager) application.getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private void updateLocationData(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        long currentTimeMillis = System.currentTimeMillis();

        if (tripData != null)
            coordinatesDetailDao.insert(new CoordinatesData(tripData.getDBid(), currentTimeMillis, latitude, longitude));
    }

    private void sendMessage(String messageInformation) {
        Intent intent = new Intent();
        intent.setAction(Const.TRACK_LOCATION_RECEIVER);
        intent.putExtra(Const.ACTION, messageInformation);
        application.sendBroadcast(intent);
    }

    private void showNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_gps_track);
        builder.setAutoCancel(true);
        builder.setContentTitle(application.getResources().getString(R.string.trackingLocalisation));
        builder.setContentText(application.getString(R.string.trackRunning));
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pi =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(pi);
        Notification notification = builder.build();
        startForeground(Const.TRACK_LOCATION, notification);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showNotificationOnOreoDevices() {
        notficationOreo = new NotificationUtils(this);
        String title = application.getResources().getString(R.string.trackingLocalisation);
        String description = application.getResources().getString(R.string.trackRunning);
        Notification.Builder builder = notficationOreo.getTrackMeNotification(title, description);
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pi =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(pi);
        notficationOreo.getManager().notify(1, builder.build());
    }

    private class LocationListener implements android.location.LocationListener {

        @Override
        public void onLocationChanged(Location location) {

            String locationString = String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude());
            if (!isFirstPositionStored) {
                isFirstPositionStored = true;
                preferences.setTripStartPoint(locationString);
            }
            preferences.getTripEndPoint(locationString);
            updateLocationData(location);
            sendMessage(Const.UPDATE);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getApplicationContext(), application.getResources().getString(R.string.gpsOn), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        preferences.setTrackingLocationService(false);
        checkSizePathPoints();
        isFirstPositionStored = false;
        tripData = null;
        cancelNotification();
        locationManager.removeUpdates(locationListeners[0]);
    }

}
