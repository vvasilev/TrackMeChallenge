package com.vasco.trackme.locationService.receiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vasco.trackme.ui.utils.ObservableObject;
//broadcast intent action when the location provider changes
public class LocationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            ObservableObject.getInstance().updateValue(intent);
        }
    }
}
