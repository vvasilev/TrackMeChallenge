package com.vasco.trackme.locationService.receiver;


public interface LocationTrackingReceiverInterface{
    void onUpdateLocation();
    void onRemoveLocation();
}
