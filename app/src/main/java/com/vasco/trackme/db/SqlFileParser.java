package com.vasco.trackme.db;


import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class SqlFileParser {

    public static final List<String> getStatements(Context context, String fileName) {
        AssetManager assetManager = context.getAssets();
        ArrayList<String> sqlStatements = new ArrayList<String>();
        InputStream fileInputStream;
        try {
            fileInputStream = assetManager.open(fileName);
        } catch (IOException ioe) {
            throw new RuntimeException("SQL file " + fileName + " not found!", ioe);
        }
        InputStreamReader isReader = new InputStreamReader(fileInputStream);
        BufferedReader fileReader = new BufferedReader(isReader);
        String line;
        String statement = "";
        try {
            while ((line = fileReader.readLine()) != null) {
                line = line.trim();
                if (line.length() > 0 && !line.startsWith("--")) {
                    statement = statement + " " + line;
                    if (line.indexOf(";") == (line.length() - 1)) {
                        sqlStatements.add(statement);
                        statement = "";
                    }
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("SQL file " + fileName + " read error!", ioe);
        }

        return sqlStatements;
    }
}
