package com.vasco.trackme.db.dao;


import android.content.ContentValues;
import android.database.Cursor;

import com.vasco.trackme.db.DBHelper;
import com.vasco.trackme.model.CoordinatesData;

public class CoordinatesDao extends BaseDao<CoordinatesData> {

    public static final String TABLE_NAME = "coordinates_info";

    public CoordinatesDao(DBHelper dbHelper) {
        super(dbHelper);
    }

    @Override
    protected CoordinatesData mapFromCursor(Cursor cursor) {
        long idDataBase = cursor.getLong(cursor.getColumnIndex(CoordinatesData.Fields.ID));
        long journeyId = cursor.getLong(cursor.getColumnIndex(CoordinatesData.Fields.TRIP_ID));
        long timestamp = cursor.getLong(cursor.getColumnIndex(CoordinatesData.Fields.TIMESTMP));
        double lat = cursor.getDouble(cursor.getColumnIndex(CoordinatesData.Fields.LAT));
        double lng = cursor.getDouble(cursor.getColumnIndex(CoordinatesData.Fields.LNG));
        return new CoordinatesData(idDataBase, journeyId, timestamp, lat, lng);
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected ContentValues mapToContentValues(CoordinatesData entity) {
        ContentValues cv = new ContentValues();
        cv.put(CoordinatesData.Fields.ID, entity.getDBid());
        cv.put(CoordinatesData.Fields.TRIP_ID, entity.getTripId());
        cv.put(CoordinatesData.Fields.TIMESTMP, entity.getTimestmp());
        cv.put(CoordinatesData.Fields.LAT, entity.getLat());
        cv.put(CoordinatesData.Fields.LNG, entity.getLng());
        return cv;
    }
}
