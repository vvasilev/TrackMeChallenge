package com.vasco.trackme.db.dao;


import android.content.ContentValues;
import android.database.Cursor;

import com.vasco.trackme.db.DBHelper;
import com.vasco.trackme.model.TripData;

public class TripDao extends BaseDao<TripData> {

    public static final String TABLE_NAME = "path_info";

    public TripDao(DBHelper dbHelper) {
        super(dbHelper);
    }

    @Override
    protected TripData mapFromCursor(Cursor cursor) {
        long idDataBase = cursor.getLong(cursor.getColumnIndex(TripData.Fields.ID));
        String name = cursor.getString(cursor.getColumnIndex(TripData.Fields.NAME));

        return new TripData(idDataBase, name);
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected ContentValues mapToContentValues(TripData entity) {
        ContentValues cv = new ContentValues();
        cv.put(TripData.Fields.ID, entity.getDBid());
        cv.put(TripData.Fields.NAME, entity.getName());
        return cv;
    }
}
