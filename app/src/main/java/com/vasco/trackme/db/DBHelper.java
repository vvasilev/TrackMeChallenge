package com.vasco.trackme.db;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;
import com.vasco.trackme.db.dao.CoordinatesDao;
import com.vasco.trackme.db.dao.TripDao;

import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    private static final String[] UPGRADE_SCRIPTS = {""};
    public static final String DATABASE_NAME = "trackMeDB.db";
    private static final String CREATE_TABLE_SCRIPT = "trackMeDB.sql";

    private CoordinatesDao coordinatesDetailDao;
    private TripDao tripDataDao;

    private Context context;

    public DBHelper(Context context, Gson gson) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        this.coordinatesDetailDao = new CoordinatesDao(this);
        this.tripDataDao = new TripDao(this);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        List<String> statements = SqlFileParser.getStatements(context, CREATE_TABLE_SCRIPT);
        if (!statements.isEmpty())
            executeStatements(sqLiteDatabase, statements);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        for (int i = oldVersion; i < newVersion; i++) {
            String upgradeScript = UPGRADE_SCRIPTS[i];
            List<String> statements = SqlFileParser.getStatements(context, upgradeScript);
            executeStatements(sqLiteDatabase, statements);
        }
    }

    public CoordinatesDao getCoordinatesDetailDao() {
        return coordinatesDetailDao;
    }

    public TripDao getTripDataDao() {
        return tripDataDao;
    }

    private void executeStatements(SQLiteDatabase db, List<String> sqlStatements) {
        db.beginTransaction();
        try {
            for (String stmt : sqlStatements) {
                try {
                    db.execSQL(stmt);
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName(), stmt, e);
                    throw new RuntimeException(e);
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }
}
