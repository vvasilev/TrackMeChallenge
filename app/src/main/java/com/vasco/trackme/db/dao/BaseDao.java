package com.vasco.trackme.db.dao;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.vasco.trackme.db.DBHelper;
import com.vasco.trackme.model.BaseEntityData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public abstract class BaseDao<T extends BaseEntityData> {

    protected DBHelper dbHelper;
    protected SQLiteDatabase database;

    public BaseDao(DBHelper dbHelper) {
        this.dbHelper = dbHelper;
        database = dbHelper.getWritableDatabase();
    }


    public long insert(T entity) {
        ContentValues data = mapToContentValues(entity);
        long id = database.insertOrThrow(getTableName(), null, data);
        entity.setDBid( id );
        return id;
    }

    public long insert(T entity, String tableName) {
        ContentValues data = mapToContentValues(entity);
        long id = database.insertOrThrow(tableName, null, data);
        entity.setDBid( id );
        return id;
    }

    public void update(T entity) {
        if (entity != null) {
            String where = "id = ?";
            String[] whereArgs = {String.valueOf( entity.getDBid() )};
            ContentValues data = mapToContentValues( entity );
            database.update( getTableName(), data, where, whereArgs );
        }
    }

    public void delete(final T entity) {
        String where = "id = ?";
        String[] whereArgs = {String.valueOf(entity.getDBid())};
        database.delete( getTableName(), where, whereArgs );
    }


    public void delete( long id ) {
        String where = "id = ?";
        String[] whereArgs = {"" + id};
        database.delete( getTableName(), where, whereArgs );
    }

    public T findById(long id) {
        String selection = "id = ?";
        String[] args = {"" + id};
        return find(selection, args);
    }

    public T find(String selection, String[] args) {
        final Cursor cursor = database.query(getTableName(), null, selection, args, null, null, null);
        T fromCursor = getFromCursor(cursor);
        close(cursor);
        return fromCursor;
    }

    public List<T> offset(int limit, int offset){
        final Cursor cursor = database.query(getTableName(), null, null, null, null, null, null, String.valueOf(offset) + "," + String.valueOf(limit) );
        return getManyFromCursor(cursor);
    }

    public List<T> findMany(String selection, String[] args) {
        List<T> result = findMany(selection, args, null, null);
        return result;
    }

    public List<T> findMany(String selection, String[] args, String limit) {
        List<T> result = findMany(selection, args, null, null);
        return result;
    }

    public List<T> findMany(String selection, String[] args, String orderBy, String size) {
        final Cursor cursor = database.query(getTableName(), null, selection, args, null, null, orderBy, size);
        return getManyFromCursor(cursor);
    }

    protected List<T> getManyFromCursor(Cursor cursor) {
        List<T> list = new ArrayList<T>();
        while (cursor.moveToNext()) {
            T entity = mapFromCursor(cursor);
            list.add(entity);
        }
        close(cursor);
        return list;
    }

    protected T getFromCursor(Cursor cursor) {
        T entity = null;
        if (cursor.moveToNext()) {
            entity = mapFromCursor(cursor);
        }
        close(cursor);
        return entity;
    }

    public void close(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    protected abstract T mapFromCursor(Cursor cursor);

    protected abstract String getTableName();

    protected abstract ContentValues mapToContentValues(T entity);

}
