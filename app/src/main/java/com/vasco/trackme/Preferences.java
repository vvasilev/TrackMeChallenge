package com.vasco.trackme;


import android.content.SharedPreferences;

import com.vasco.trackme.ui.utils.Const;

public class Preferences {

    public static final String TRIP_ID = "tripId";
    public static final String TRIP_START_POINT = "tripStartPoint";
    public static final String TRIP_END_POINT = "tripEndPoint";
    public static final String COORDS_TIMESTMP = "coordTimeStmp";
    public static final String TRACKING_ON = "trackingIsEnabled";

    private SharedPreferences preferences;

    public void setTripId(long tripId) {
        setValue(tripId, TRIP_ID);
    }

    public long getTripId(){
        return preferences.getLong(TRIP_ID, Const.DEFAULT_VALUE_LONG);
    }

    public void setTripStartPoint(String position){
        setValue(position, TRIP_START_POINT);
    }

    public String getTripStartPoint(){
        return preferences.getString(TRIP_START_POINT, "");
    }

    public void getTripEndPoint(String position){
        setValue(position, TRIP_END_POINT);
    }

    public String getTripEndPoint(){
        return preferences.getString(TRIP_END_POINT, "");
    }


    public Preferences(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public void setTrackingLocationService(boolean tracking){
        setValue(tracking, TRACKING_ON);
    }

    public boolean isTrackingLocationService(){
        return preferences.getBoolean(TRACKING_ON, false);
    }

    public void setCoordinatesTimestamp(long coordinatesTimestamp) {
        setValue(coordinatesTimestamp, COORDS_TIMESTMP);
    }

    public long getCoordinatesTimestamp(){
        return preferences.getLong(COORDS_TIMESTMP, Const.DEFAULT_VALUE_LONG);
    }


    private void setValue(String value, String key) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private void setValue(boolean value, String key) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean( key, value );
        editor.apply();
    }

    private void setValue(long value, String key) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    private void setValue(int value, String key) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }
}
