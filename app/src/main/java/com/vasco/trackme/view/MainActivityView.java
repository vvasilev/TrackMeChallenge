package com.vasco.trackme.view;


import android.support.v4.app.Fragment;

public interface MainActivityView {

    void replaceFragment(Fragment currentFragment);

    Fragment getCurrentFragment();
}
