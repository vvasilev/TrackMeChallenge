package com.vasco.trackme.view;


import android.view.View;
import com.vasco.trackme.activities.BaseActivity;

public interface BaseView {

    BaseActivity getBaseActivity();

    void showError(Throwable e);

    void showSnackbar(View v, String message);



}
