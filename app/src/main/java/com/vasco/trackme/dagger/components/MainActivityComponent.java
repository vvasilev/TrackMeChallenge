package com.vasco.trackme.dagger.components;

import com.vasco.trackme.activities.MainActivity;
import com.vasco.trackme.dagger.modules.MainActivityModule;
import com.vasco.trackme.dagger.scopes.MainActivityScope;

import dagger.Component;

@MainActivityScope
@Component(modules = MainActivityModule.class, dependencies = AppComponent.class)
public interface MainActivityComponent {

    void injectActivity(MainActivity mActivity);

}
