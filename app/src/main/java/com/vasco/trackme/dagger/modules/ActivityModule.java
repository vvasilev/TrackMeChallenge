package com.vasco.trackme.dagger.modules;

import android.app.Activity;

import com.vasco.trackme.dagger.scopes.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @ApplicationScope
    public Activity activity() {
        return activity;
    }
}
