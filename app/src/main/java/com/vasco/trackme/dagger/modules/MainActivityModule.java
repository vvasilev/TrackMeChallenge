package com.vasco.trackme.dagger.modules;

import com.squareup.picasso.Picasso;
import com.vasco.trackme.activities.MainActivity;
import com.vasco.trackme.Preferences;
import com.vasco.trackme.db.DBHelper;
import com.vasco.trackme.dagger.scopes.MainActivityScope;
import com.vasco.trackme.locationService.ServerRequest;
import com.vasco.trackme.ui.viewModels.MainController;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    private final MainActivity activity;

    public MainActivityModule(MainActivity activity){
        this.activity = activity;
    }

    @Provides
    @MainActivityScope
    public MainController getMainController(ServerRequest serverRequest, DBHelper dbHelper,
                                            Preferences preferences, Picasso picasso){
        return new MainController(serverRequest, dbHelper, preferences, activity, picasso);
    }
}
