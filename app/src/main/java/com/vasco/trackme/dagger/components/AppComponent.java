package com.vasco.trackme.dagger.components;

import com.squareup.picasso.Picasso;
import com.vasco.trackme.Preferences;
import com.vasco.trackme.db.DBHelper;
import com.vasco.trackme.dagger.modules.ActivityModule;
import com.vasco.trackme.dagger.modules.PrefsModule;
import com.vasco.trackme.dagger.modules.ServerRequestModule;
import com.vasco.trackme.dagger.modules.DBModule;
import com.vasco.trackme.dagger.modules.PicasoModule;
import com.vasco.trackme.dagger.scopes.ApplicationScope;
import com.vasco.trackme.locationService.ServerRequest;

import dagger.Component;

@ApplicationScope
@Component(modules = {ServerRequestModule.class, DBModule.class,
        PrefsModule.class, ActivityModule.class, PicasoModule.class})
public interface AppComponent {

    DBHelper getDBHelper();

    Preferences getPreferences();

    ServerRequest getServerRequest();

    Picasso getPicasso();
}
