package com.vasco.trackme.dagger.modules;

import com.fatboyindustrial.gsonjodatime.DateTimeConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vasco.trackme.dagger.scopes.ApplicationScope;
import com.vasco.trackme.locationService.ServerRequest;
import com.vasco.trackme.locationService.ServerRequestImpl;
import com.vasco.trackme.ui.utils.Const;

import org.joda.time.DateTime;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module (includes = LocationServiceModule.class)
public class ServerRequestModule {

    @Provides
    public ServerRequest getCommunicationService(Retrofit retrofit){
        return new ServerRequestImpl(retrofit);
    }

    @Provides
    @ApplicationScope
    public Gson getGson(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeConverter());
        return gsonBuilder.create();
    }

    @Provides
    @ApplicationScope
    public Retrofit getRetrofit(OkHttpClient okHttpClient, Gson gson){
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(Const.BASIC_API)
                .client(okHttpClient)
                .build();
    }

}
