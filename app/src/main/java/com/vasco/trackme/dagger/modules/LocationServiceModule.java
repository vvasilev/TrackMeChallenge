package com.vasco.trackme.dagger.modules;

import android.content.Context;

import com.vasco.trackme.dagger.scopes.ApplicationScope;

import java.io.File;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

@Module(includes = CtxModule.class)
public class LocationServiceModule {

    private static final int CACHE = 10 * 1024 * 1024; // 10MB

    @Provides
    @ApplicationScope
    public File getCacheFile(Context context) {
        File file = new File(context.getCacheDir(), "trip_data");
        file.mkdir();
        return file;
    }

    @Provides
    @ApplicationScope
    public Cache getCache(File file) {
        return new Cache(file, CACHE);
    }

    @Provides
    @ApplicationScope
    public HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(message -> Timber.i(message));
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return loggingInterceptor;
    }

    @Provides
    @ApplicationScope
    public Interceptor getInterceptor() {
        return chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Accept", "application/json")
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        };
    }

    @Provides
    @ApplicationScope
    public OkHttpClient getOkHttpClient(HttpLoggingInterceptor loggingInterceptor, Interceptor interceptor, Cache cache) {
        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(interceptor)
                .cache(cache)
                .build();
    }


}
