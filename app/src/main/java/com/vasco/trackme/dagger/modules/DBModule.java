package com.vasco.trackme.dagger.modules;

import android.content.Context;

import com.google.gson.Gson;
import com.vasco.trackme.db.DBHelper;
import com.vasco.trackme.dagger.scopes.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module(includes = { CtxModule.class, ServerRequestModule.class })
public class DBModule {

    @Provides
    @ApplicationScope
    public DBHelper getDBHelper(Context ctx, Gson gson){
        return new DBHelper(ctx, gson);
    }
}
