package com.vasco.trackme.dagger.modules;

import android.content.Context;

import com.vasco.trackme.dagger.scopes.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class CtxModule {

    private final Context ctx;

    public CtxModule(Context ctx) {
        this.ctx = ctx.getApplicationContext();
    }

    @Provides
    @ApplicationScope
    public Context getCtx() {
        return ctx;
    }
}
