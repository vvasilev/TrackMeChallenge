package com.vasco.trackme.dagger.modules;

import android.content.Context;
import android.content.SharedPreferences;

import com.vasco.trackme.Preferences;
import com.vasco.trackme.dagger.scopes.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module(includes = CtxModule.class)
public class PrefsModule {

    private static final String SHARED_PREFERENCES = "TrackMePreferences";

    @Provides
    @ApplicationScope
    public Preferences getAppPreferences(SharedPreferences preferences){
        return new Preferences(preferences);
    }

    @Provides
    @ApplicationScope
    public SharedPreferences getSharedPreferences(Context context){
        return context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }
}
