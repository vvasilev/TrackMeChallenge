package com.vasco.trackme.dagger.components;

import com.vasco.trackme.dagger.modules.TripModule;
import com.vasco.trackme.dagger.scopes.TripScope;
import com.vasco.trackme.ui.fragment.TripFragment;

import dagger.Component;

@TripScope
@Component(modules = TripModule.class, dependencies = AppComponent.class)
public interface TripComponent {

    void injectFragment(TripFragment tripFragmentFragment);
}
