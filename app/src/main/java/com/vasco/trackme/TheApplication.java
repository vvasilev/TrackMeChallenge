package com.vasco.trackme;


import android.app.Application;

import com.squareup.picasso.Picasso;
import com.vasco.trackme.db.DBHelper;
import com.vasco.trackme.dagger.components.AppComponent;
import com.vasco.trackme.dagger.components.DaggerAppComponent;
import com.vasco.trackme.dagger.modules.CtxModule;

import timber.log.Timber;

public class TheApplication extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        doTheGraph();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static DBHelper getDBHelper() {
        return appComponent.getDBHelper();
    }

    public static Picasso getPicasso() {
        return appComponent.getPicasso();
    }

    public static Preferences getPreferences() {
        return appComponent.getPreferences();
    }

    private void doTheGraph() {
        appComponent = DaggerAppComponent.builder()
                .ctxModule(new CtxModule(this))
                .build();
    }
}
