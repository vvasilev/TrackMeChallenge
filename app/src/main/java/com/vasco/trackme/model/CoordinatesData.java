package com.vasco.trackme.model;


import android.os.Parcel;
import android.support.annotation.NonNull;

public class CoordinatesData extends BaseEntityData implements Comparable<CoordinatesData> {

    public interface Fields extends BaseEntityData.Fields {
        String TRIP_ID = "path_id";
        String LAT = "coord_lat";
        String LNG = "coord_lng";
        String TIMESTMP = "time_stm";
    }

    private double lat;
    private double lng;

    private long tripId;
    private long timestmp;

    public CoordinatesData(long tripId, long timestamp, double lat, double lng) {
        this.tripId = tripId;
        this.timestmp = timestamp;
        this.lat = lat;
        this.lng = lng;
    }

    public CoordinatesData(Long idDataBase, long tripId, long timestamp, double lat, double lng) {
        super(idDataBase);
        this.tripId = tripId;
        this.timestmp = timestamp;
        this.lat = lat;
        this.lng = lng;
    }

    public CoordinatesData(Parcel in) {
        super(in);
        this.tripId = in.readLong();
        this.timestmp = in.readLong();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
    }

    //Getters and Setters
    public long getTripId() {
        return tripId;
    }

    public void setTripId(long tripId) {
        this.tripId = tripId;
    }

    public long getTimestmp() {
        return timestmp;
    }

    public void setTimestmp(long timestmp) {
        this.timestmp = timestmp;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public static final Creator<CoordinatesData> CREATOR = new Creator<CoordinatesData>() {
        @Override
        public CoordinatesData createFromParcel(Parcel in) {
            return new CoordinatesData(in);
        }

        @Override
        public CoordinatesData[] newArray(int size) {
            return new CoordinatesData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeLong(tripId);
        parcel.writeLong(timestmp);
        parcel.writeDouble(lat);
        parcel.writeDouble(lng);
    }

    @Override
    public int compareTo(@NonNull CoordinatesData coordinatesDetail) {
        return (int) (timestmp - coordinatesDetail.getTimestmp());
    }

}
