package com.vasco.trackme.model;


import android.os.Parcel;

public class TripData extends BaseEntityData {

    public interface Fields extends BaseEntityData.Fields {
        String NAME = "name";
    }

    private String name;

    public TripData(String name) {
        this.name = name;
    }

    public TripData(Long DBid, String name) {
        super(DBid);
        this.name = name;
    }

    public TripData(Parcel in) {
        super(in);
        this.name = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static final Creator<TripData> CREATOR = new Creator<TripData>() {
        @Override
        public TripData createFromParcel(Parcel in) {
            return new TripData(in);
        }

        @Override
        public TripData[] newArray(int size) {
            return new TripData[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(name);
    }

}
