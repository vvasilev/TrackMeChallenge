package com.vasco.trackme.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.vasco.trackme.ui.utils.Const;

public abstract class BaseEntityData implements Parcelable {

    public interface Fields {
        String ID = "id";
    }

    protected Long DBid;

    public BaseEntityData() {
    }

    public BaseEntityData(Long DBid) {
        this.DBid = DBid;
    }

    protected BaseEntityData(Parcel in) {
        DBid = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(DBid == null ? Const.DEFAULT_VALUE_LONG : DBid);
    }


    public Long getDBid() {
        return DBid;
    }

    public void setDBid(Long DBid) {
        this.DBid = DBid;
    }

    public boolean hasId() {
        return DBid != null;
    }
}
