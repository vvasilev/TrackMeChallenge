package com.vasco.trackme.activities;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.vasco.trackme.R;
import com.vasco.trackme.view.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    @Nullable
    @BindView(R.id.loading_journeyData)
    ProgressBar progressBar;

    protected abstract int getLayout();
    protected abstract void init(Bundle savedInstanceState);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);
        init(savedInstanceState);
    }

    public void showProgress( boolean visible) {
        if(progressBar != null)
            progressBar.setVisibility( visible ? VISIBLE : GONE);
    }


    @Override
    public BaseActivity getBaseActivity() {
        return this;
    }

    @Override
    public void showError(Throwable e) {

    }

    @Override
    public void showSnackbar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
    }
}
