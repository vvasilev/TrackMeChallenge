package com.vasco.trackme.activities;

import android.Manifest;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.Button;
import android.widget.Switch;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.jakewharton.rxbinding.view.RxView;
import com.vasco.trackme.R;
import com.vasco.trackme.TheApplication;
import com.vasco.trackme.dagger.components.DaggerMainActivityComponent;
import com.vasco.trackme.dagger.modules.MainActivityModule;
import com.vasco.trackme.locationService.receiver.LocationTrackingReceiverInterface;
import com.vasco.trackme.model.TripData;
import com.vasco.trackme.locationService.receiver.LocationTrackingReceiver;
import com.vasco.trackme.ui.adapter.TripAdapter;
import com.vasco.trackme.ui.viewModels.MainController;
import com.vasco.trackme.ui.fragment.TripFragment;
import com.vasco.trackme.ui.utils.Const;
import com.vasco.trackme.ui.utils.TripPaginationState;
import com.vasco.trackme.ui.utils.ObservableObject;
import com.vasco.trackme.view.MainActivityView;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;

public class MainActivity extends BaseActivity implements MainActivityView, OnMapReadyCallback, Observer,
        TripFragment.TripListInterface, TripAdapter.JourneyAdapterInterface,
        LocationTrackingReceiverInterface {


    @BindView(R.id.startTracking)
    Button clearMap;  //for debugging

    @BindView(R.id.switch1)
    Switch trackSwitch;

    @Inject
    MainController controller;

    private GoogleMap gMap;
    private SupportMapFragment mapFragment;
    private LocationTrackingReceiver trackLocationReceiver;

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        doTheGraph();
        super.onCreate(savedInstanceState);

        RxView.clicks(trackSwitch).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe(x ->
                controller.onClickTrackingButton(gMap));

        RxView.clicks(clearMap).throttleFirst(500, TimeUnit.MILLISECONDS).subscribe(x ->
                controller.cleanTheMap(gMap));

        registerReceiver(trackLocationReceiver, new IntentFilter(Const.TRACK_LOCATION_RECEIVER));
        ObservableObject.getInstance().addObserver(this);
    }

    private void doTheGraph() {
        DaggerMainActivityComponent.builder()
                .mainActivityModule(new MainActivityModule(this))
                .appComponent(TheApplication.getAppComponent())
                .build()
                .injectActivity(this);
    }


    @Override
    protected void init(Bundle savedInstanceState) {
        controller.setView(this, savedInstanceState);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        trackLocationReceiver = new LocationTrackingReceiver();
        startMapFragment();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        controller.onSaveInstanceState(outState);
    }


    private void startMapFragment() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (controller.checkLocationPermission())
                mapFragment.getMapAsync(this);
        } else {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Const.REQUEST_LOCATION_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mapFragment.getMapAsync(this);
                    }
                } else {
                    // permission denied
                    showSnackbar(getCurrentFocus(), getResources().getString(R.string.permissionDenied));
                }
                return;
            }

        }
    }


    @Override
    public void update(Observable observable, Object o) {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showSnackbar(getCurrentFocus(), getResources().getString(R.string.noGps));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.setOnMarkerClickListener(marker -> true);
        gMap.setMyLocationEnabled(true);
    }

    @Override
    public void replaceFragment(Fragment currentFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragmentLayout, currentFragment, controller.getCurrentFragmentTag()).commit();
        fragmentManager.executePendingTransactions();
    }

    @Override
    public Fragment getCurrentFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        return fragmentManager
                .findFragmentByTag(controller.getCurrentFragmentTag());
    }

    @Override
    public void onUpdateLocation() {
        controller.onUpdateLocation(gMap);
    }

    @Override
    public void onRemoveLocation() {
        controller.cleanTheMap(gMap);
        controller.removeLastLocationFromAdapter();
    }

    @Override
    public void onTripClick(int position, List<TripData> tripData) {
        if (tripData.get(position) != null && !controller.isTrackingLoationService()) {
            controller.showSelectedTripOnMap(tripData.get(position), gMap);
        } else {
            showSnackbar(getCurrentFocus(), getResources().getString(R.string.trackingModeON));
        }
    }

    @Override
    public void onTripLoad(TripPaginationState tripPaginationState) {
        controller.onLoadTrip(tripPaginationState);
    }

    @Override
    public boolean isLoadingTrip() {
        return controller.isLoadingTrip();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        controller.onDestroy();
        unregisterReceiver(trackLocationReceiver);
    }
}
