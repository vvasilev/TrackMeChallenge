package com.vasco.trackme.ui.fragment;


import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.vasco.trackme.R;
import com.vasco.trackme.ui.utils.RecyclerViewStateSupport;
import com.vasco.trackme.ui.utils.WrapContentLinearLayoutManager;

import butterknife.BindView;

public abstract class BasePagination extends BaseFragment {

    @BindView(R.id.rv)
    RecyclerViewStateSupport list;

    protected abstract void createAdapter();
    protected abstract RecyclerView.Adapter getAdapter();
    protected abstract View getEmptyView();
    protected abstract View getErrorView();
    protected abstract View getLoadingView();

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRecyclerView();
    }

    protected void refreshAdapter(){
        setLoading(false);
    }

    protected void setRecyclerView(){
        createAdapter();
        list.setLayoutManager(new WrapContentLinearLayoutManager(activity));
        list.setViews(getEmptyView(), getLoadingView(), getErrorView());
        list.setAdapter(getAdapter());
    }

    public void setLoading(boolean loading){
        list.setLoading(loading);
    }

    public void notifyDataSetChanged(){
        list.notifyDataSetChanged();
    }

    @Override
    public void showError(Throwable e) {
        list.setHasError(true);
    }
}
