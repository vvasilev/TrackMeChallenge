package com.vasco.trackme.ui.utils;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public abstract class BasePaginationState implements Parcelable {

    private int page;
    private int previousTotalItemCount;
    private boolean loading = true;
    private int visibleThreshold;
    private int firstVisibleItem;
    private int visibleItemCount;
    private int totalItemCount;
    private int startingPageIndex;
    private int currentPage;

    public abstract List<?> getList();

    public BasePaginationState() {
        this.page = 0;
    }

    protected BasePaginationState(Parcel in) {
        page = in.readInt();
        previousTotalItemCount = in.readInt();
        loading = in.readByte() != 0;
        visibleThreshold = in.readInt();
        firstVisibleItem = in.readInt();
        visibleItemCount = in.readInt();
        totalItemCount = in.readInt();
        startingPageIndex = in.readInt();
        currentPage = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(page);
        parcel.writeInt(previousTotalItemCount);
        parcel.writeByte((byte) (loading ? 1 : 0));
        parcel.writeInt(visibleThreshold);
        parcel.writeInt(firstVisibleItem);
        parcel.writeInt(visibleItemCount);
        parcel.writeInt(totalItemCount);
        parcel.writeInt(startingPageIndex);
        parcel.writeInt(currentPage);
    }

    public int getPage() {
        return page;
    }

    public void increaseCurrentPage(){
        currentPage++;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPreviousTotalItemCount() {
        return previousTotalItemCount;
    }

    public void setPreviousTotalItemCount(int previousTotalItemCount) {
        this.previousTotalItemCount = previousTotalItemCount;
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public int getVisibleThreshold() {
        return visibleThreshold;
    }

    public void setVisibleThreshold(int visibleThreshold) {
        this.visibleThreshold = visibleThreshold;
    }

    public int getFirstVisibleItem() {
        return firstVisibleItem;
    }

    public void setFirstVisibleItem(int firstVisibleItem) {
        this.firstVisibleItem = firstVisibleItem;
    }

    public int getVisibleItemCount() {
        return visibleItemCount;
    }

    public void setVisibleItemCount(int visibleItemCount) {
        this.visibleItemCount = visibleItemCount;
    }

    public int getTotalItemCount() {
        return totalItemCount;
    }

    public void setTotalItemCount(int totalItemCount) {
        this.totalItemCount = totalItemCount;
    }

    public int getStartingPageIndex() {
        return startingPageIndex;
    }

    public void setStartingPageIndex(int startingPageIndex) {
        this.startingPageIndex = startingPageIndex;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void clearAllState(){
        page = 0;
        previousTotalItemCount = -1;
        loading = false;
        visibleThreshold = 0;
        firstVisibleItem = 0;
        visibleItemCount = 0;
        totalItemCount = 0;
        startingPageIndex = 0;
        currentPage = 0;
        getList().clear();
    }
}
