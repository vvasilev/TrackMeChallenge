package com.vasco.trackme.ui.viewModels;


import android.app.Activity;
import android.os.Bundle;

import com.squareup.picasso.Picasso;
import com.vasco.trackme.Preferences;
import com.vasco.trackme.activities.BaseActivity;
import com.vasco.trackme.db.DBHelper;
import com.vasco.trackme.locationService.ServerRequest;
import com.vasco.trackme.view.BaseView;

public abstract class BaseController<T extends BaseView> {

    protected T view;

    protected Activity activity;
    protected Preferences prefs;
    protected String currentFragmentTag;
    protected Picasso picasso;
    protected ServerRequest serverRequest;
    protected DBHelper dbHelper;



    protected abstract void init(Bundle savedInstanceState);

    public BaseController(ServerRequest serverRequest, DBHelper dbHelper,
                          Preferences prefs, Activity activity, Picasso picasso) {
        this.serverRequest = serverRequest;
        this.dbHelper = dbHelper;
        this.prefs = prefs;
        this.activity = activity;
        this.picasso = picasso;
    }

    public void setView(T view, Bundle savedInstanceState) {
        this.view = view;
        if (view != null) {
            init(savedInstanceState);
        }
    }


    public void setShowProgressInTask(boolean progress) {
        BaseActivity activity = view.getBaseActivity();
        if (activity != null) {
            activity.showProgress(progress);
            activity.supportInvalidateOptionsMenu();
        }
    }
}
