package com.vasco.trackme.ui.utils;


public class Const {
    public static final String TRIP_LIST_TAG = "tripListTag";

    public static final long DEFAULT_VALUE_LONG = -1L;
    public static final int REQUEST_LOCATION_PERMISSIONS = 1;
    public static final int LIMIT_OFFSET = 20;

    //network
    public static final int TRACK_LOCATION = 1;
    public static final String BASIC_API = "https://maps.googleapis.com/maps/api/";

    public static final String TRACK_LOCATION_RECEIVER = "ccom.vasco.trackme.network.receiver.LocationTrackingReceiver.TRACK_LOCATION_RECEIVER";
    public static final String ACTION = "action";
    public static final String UPDATE = "update";
    public static final String REMOVE = "remove";

}
