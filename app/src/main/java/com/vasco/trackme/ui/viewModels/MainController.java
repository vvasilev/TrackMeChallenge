package com.vasco.trackme.ui.viewModels;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;
import com.vasco.trackme.Preferences;
import com.vasco.trackme.R;
import com.vasco.trackme.activities.MainActivity;
import com.vasco.trackme.db.DBHelper;
import com.vasco.trackme.db.dao.CoordinatesDao;
import com.vasco.trackme.db.dao.TripDao;
import com.vasco.trackme.model.CoordinatesData;
import com.vasco.trackme.model.TripData;
import com.vasco.trackme.locationService.ServerRequest;
import com.vasco.trackme.locationService.service.LocationTrackingService;
import com.vasco.trackme.ui.fragment.TripFragment;
import com.vasco.trackme.ui.utils.Const;
import com.vasco.trackme.ui.utils.TripPaginationState;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static rx.schedulers.Schedulers.io;

public class MainController extends BaseController<MainActivity> {

    private static final String FRAGMENT_TAG = "fragmentTag";

    private List<Polyline> path;
    private PolylineOptions polylineOptions;

    private boolean setStartPoint;
    private boolean isLodingTrip;

    public MainController(ServerRequest serverRequest, DBHelper databaseHelper,
                          Preferences appPreferences, Activity activity, Picasso picasso) {
        super(serverRequest, databaseHelper, appPreferences, activity, picasso);
    }


    @Override
    protected void init(Bundle savedInstanceState) {

        if (savedInstanceState == null) {
            polylineOptions = createPolyline();
            path = new ArrayList<>();
            TripFragment tripList = new TripFragment();
            settingUpFragment(tripList, Const.TRIP_LIST_TAG);
        }else {
            setSavedInstanceState(savedInstanceState);
        }
    }

    public void setSavedInstanceState(Bundle savedInstanceState) {
        currentFragmentTag = savedInstanceState.getString(FRAGMENT_TAG);
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putString(FRAGMENT_TAG, currentFragmentTag);
    }

    private void settingUpFragment(Fragment fragment, String fragmentTag){
        currentFragmentTag = fragmentTag;
        view.replaceFragment(fragment);
    }

    private PolylineOptions createPolyline(){
        PolylineOptions options = new PolylineOptions();
        options.color(ContextCompat.getColor(activity, R.color.red));
        options.width(8);
        options.visible( true );
        return options;
    }

    public void onClickTrackingButton(GoogleMap gMap) {
        if(isTrackingLoationService()){
            stopTracking();
            setEndPoint(gMap);
            addItemToList(prefs.getTripId());
        }else {
            if (GpsAndConnectionChecking()){
                setStartPoing(false);
                cleanTheMap(gMap);
                startTracking();
            } else {
                dialogToEnableGpsAndNetwork();
            }

        }
    }

    private void addItemToList(long tripId) {
        Fragment currentFragment = view.getCurrentFragment();
        if (currentFragment != null && currentFragment instanceof TripFragment){
            TripDao journeyDetailDao = dbHelper.getTripDataDao();
            ((TripFragment) currentFragment).addTripToList(journeyDetailDao.findById(tripId));
        }
    }

    private void startTracking(){
        Intent intent  = new Intent(activity.getApplicationContext(), LocationTrackingService.class);
        activity.startService(intent);
    }

    private void stopTracking(){
        Intent myService = new Intent(activity, LocationTrackingService.class);
        activity.stopService(myService);
    }

    public String getCurrentFragmentTag() {
        return currentFragmentTag;
    }

    public void cleanPolylinePath(GoogleMap gMap) {
        if (path != null){
            Iterator<Polyline> polylineIterator = path.iterator();
            while (polylineIterator.hasNext()){
                Polyline polyline = polylineIterator.next();
                polyline.remove();
            }
            polylineOptions = createPolyline();
            gMap.addPolyline(polylineOptions);
        }
    }

    private void addMarker(String title, LatLng position, GoogleMap map){
        Marker marker = map.addMarker(new MarkerOptions().title(title).position(position)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        marker.showInfoWindow();
    }

    public void setStartPoing(boolean setStartPoint) {
        this.setStartPoint = setStartPoint;
    }

    public void setEndPoint(GoogleMap gMap) {
        if (gMap != null){
            String tripEndPosition = prefs.getTripEndPoint();
            if (!tripEndPosition.isEmpty()){
                String[] latlong =  tripEndPosition.split(",");
                LatLng coordinatesEndPosition = new LatLng(Double.parseDouble(latlong[0]),
                        Double.parseDouble(latlong[1]));
                addMarker(activity.getResources().getString(R.string.endPoint), coordinatesEndPosition, gMap);
            }
        }
    }

    public void cleanTheMap(GoogleMap gMap) {
        if (gMap != null){
            gMap.clear();
            cleanPolylinePath(gMap);
        }
    }

    public void removeLastLocationFromAdapter() {
        Fragment currentFragment = view.getCurrentFragment();
        if (currentFragment != null && currentFragment instanceof TripFragment)
            ((TripFragment) currentFragment).removeItemFromListById(prefs.getTripId());
    }

    public boolean isLoadingTrip() {
        return isLodingTrip;
    }

    public boolean GpsAndConnectionChecking() {
        if (connectionAvailability() && GpsAvailability() && isLocationEnabled()){
            return true;
        }else {
            return false;
        }
    }

    private boolean connectionAvailability() {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    private boolean GpsAvailability(){
        final LocationManager manager = (LocationManager) activity.getSystemService( Context.LOCATION_SERVICE );
        return manager.isProviderEnabled( LocationManager.GPS_PROVIDER );
    }

    private boolean isLocationEnabled() {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(activity.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public void dialogToEnableGpsAndNetwork() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.permisionNetworkAndGps)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        activity.startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        Const.REQUEST_LOCATION_PERMISSIONS);

            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        Const.REQUEST_LOCATION_PERMISSIONS);
            }
            return false;
        } else {
            return true;
        }
    }

    public boolean isTrackingLoationService() {
        return prefs.isTrackingLocationService();
    }

    public void onLoadTrip(TripPaginationState state) {
        isLodingTrip = true;
        TripDao journeyDetailDao = dbHelper.getTripDataDao();
        setShowProgressInTask(true);
        Observable.just(state)
                .subscribeOn(io())
                .flatMap(new Func1<TripPaginationState, Observable<List<TripData>>>() {
                    @Override
                    public Observable<List<TripData>> call(TripPaginationState state) {
                        List<TripData> journeys = journeyDetailDao.offset(Const.LIMIT_OFFSET, state.getOffset());
                        removeCurrentTripIfTracking(journeys);
                        return Observable.from(journeys).toList();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<TripData>>() {
                    @Override
                    public void onCompleted() {
                        isLodingTrip = false;
                        setShowProgressInTask(false);
                        if (!state.getList().isEmpty()){
                            state.getList().remove(null);
                        }
                        state.setOffset(state.getOffset() + Const.LIMIT_OFFSET);
                        Fragment fragment = view.getCurrentFragment();
                        if (fragment instanceof TripFragment) {
                            ((TripFragment) fragment).refreshAdapterList();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        isLodingTrip = false;
                        setShowProgressInTask(false);
                        view.showError(new Exception());
                        if (!state.getList().isEmpty()){
                            state.getList().remove(null);
                        }
                    }

                    @Override
                    public void onNext(List<TripData> journeyDetailList) {
                        state.getTripData().addAll(journeyDetailList);
                    }
                });
    }

    public void showSelectedTripOnMap(TripData journeyDetail, GoogleMap gMap) {
        CoordinatesDao coordinatesDetailDao = dbHelper.getCoordinatesDetailDao();
        setShowProgressInTask(true);
        Observable.just(journeyDetail)
                .subscribeOn(io())
                .flatMap(new Func1<TripData, Observable<List<CoordinatesData>>>() {
                    @Override
                    public Observable<List<CoordinatesData>> call(TripData tripDetail) {
                        String selection = CoordinatesData.Fields.TRIP_ID + "=?";
                        String selectionArgs[] = {String.valueOf(tripDetail.getDBid())};
                        return  Observable.from(coordinatesDetailDao.findMany(selection, selectionArgs)).toList();
                    }
                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<CoordinatesData>>() {
                    @Override
                    public void onCompleted() {
                        setShowProgressInTask(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setShowProgressInTask(false);
                        view.showError(new Exception());
                    }

                    @Override
                    public void onNext(List<CoordinatesData> coordinatesDetails) {
                        cleanTheMap(gMap);
                        if (coordinatesDetails.size() != 0){
                            for (CoordinatesData coordinates : coordinatesDetails){
                                LatLng position = new LatLng(coordinates.getLat(), coordinates.getLng());
                                drawPolylineOnMap(gMap, position);
                                gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
                            }
                            addMarker(activity.getResources().getString(R.string.startPoint), new LatLng(coordinatesDetails.get(0).getLat(), coordinatesDetails.get(0).getLng()), gMap);
                            addMarker(activity.getResources().getString(R.string.endPoint), new LatLng(coordinatesDetails.get(coordinatesDetails.size() - 1).getLat(), coordinatesDetails.get(coordinatesDetails.size() - 1).getLng()), gMap);

                        }
                    }
                });
    }

    private void removeCurrentTripIfTracking(List<TripData> trips){
        if (prefs.isTrackingLocationService()){
            long journeyId = prefs.getTripId();
            for (int i = 0; i < trips.size(); i++){
                TripData journeyDetail = trips.get(i);
                if (journeyDetail.getDBid() == journeyId){
                    trips.remove(i);
                }
            }
        }
    }

    public void onUpdateLocation(GoogleMap gMap) {
        CoordinatesDao coordinatesDetailDao = dbHelper.getCoordinatesDetailDao();
        String selection = CoordinatesData.Fields.TRIP_ID + " = ? AND "
                + CoordinatesData.Fields.TIMESTMP + " > ?";
        String[] selectionArgs = new String[]{String.valueOf(prefs.getTripId()),
                String.valueOf(prefs.getCoordinatesTimestamp())};
        Observable.from(coordinatesDetailDao.findMany(selection, selectionArgs))
                .subscribeOn(Schedulers.newThread())
                .map(coordinateDetails -> {
                    LatLng coordinates = new LatLng(coordinateDetails.getLat(), coordinateDetails.getLng());
                    prefs.setCoordinatesTimestamp(coordinateDetails.getTimestmp());
                    return coordinates;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LatLng>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.i("ERROS", e.toString());
                    }

                    @Override
                    public void onNext(LatLng coordinates) {
                        if (!setStartPoint){
                            setStartPoint = true;
                            String journeyStartPosition = prefs.getTripStartPoint();
                            String[] latlong =  journeyStartPosition.split(",");
                            LatLng coordinatesFirstPosition = new LatLng(Double.parseDouble(latlong[0]),
                                    Double.parseDouble(latlong[1]));
                            addMarker(activity.getResources().getString(R.string.startPoint), coordinatesFirstPosition, gMap);
                        }
                        updateCameraAsUserMoves(gMap, coordinates);
                        drawPolylineOnMap(gMap, coordinates);
                    }
                });
    }

    private void updateCameraAsUserMoves(GoogleMap gMap, LatLng position) {
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
    }

    private void drawPolylineOnMap(GoogleMap gMap, LatLng position){
        polylineOptions.add(position);
        path.add(gMap.addPolyline(polylineOptions));
    }

    public void onDestroy() {
        prefs.setCoordinatesTimestamp(0);
    }

}
