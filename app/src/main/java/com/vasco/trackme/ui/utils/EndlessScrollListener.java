package com.vasco.trackme.ui.utils;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class EndlessScrollListener extends RecyclerView.OnScrollListener {

    private BasePaginationState state;

    public abstract boolean onLoadMore(int page);

    public EndlessScrollListener(BasePaginationState state) {
        this.state = state;
    }

    @Override
    public void onScrolled(RecyclerView mRecyclerView, int dx, int dy)
    {
        super.onScrolled(mRecyclerView, dx, dy);
        LinearLayoutManager mLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();

        this.state.setVisibleItemCount(mRecyclerView.getChildCount());
        this.state.setTotalItemCount(mLayoutManager.getItemCount());
        this.state.setVisibleThreshold(mLayoutManager.findFirstVisibleItemPosition());

        onScroll(this.state.getFirstVisibleItem(), this.state.getVisibleItemCount(), this.state.getTotalItemCount());
    }

    public void onScroll(int firstVisibleItem, int visibleItemCount, int totalItemCount)
    {

        if (totalItemCount < this.state.getPreviousTotalItemCount())
        {
            this.state.setCurrentPage(this.state.getStartingPageIndex());
            this.state.setPreviousTotalItemCount(totalItemCount);
            if (totalItemCount == 0)
            {
                this.state.setLoading(true);
            }
        }

        if (this.state.isLoading() && (totalItemCount > this.state.getPreviousTotalItemCount()))
        {
            this.state.setLoading(false);
            this.state.setPreviousTotalItemCount(totalItemCount);
            this.state.increaseCurrentPage();
        }

        if (!this.state.isLoading() && (totalItemCount  - visibleItemCount) <= (firstVisibleItem +
                this.state.getVisibleThreshold()))
        {
            this.state.setPreviousTotalItemCount(totalItemCount + 1);
            this.state.setLoading(true);
            this.state.getList().add(null);
            onLoadMore(this.state.getCurrentPage() + 1);
        }
    }
}
