package com.vasco.trackme.ui.utils;


import android.os.Parcel;
import android.os.Parcelable;

import com.vasco.trackme.model.TripData;

import java.util.ArrayList;
import java.util.List;

public class TripPaginationState extends BasePaginationState {

    private List<TripData> tripData;
    private int offset;

    public TripPaginationState() {
        super();
        this.tripData = new ArrayList<>();
    }

    protected TripPaginationState(Parcel in) {
        super(in);
        if (tripData == null)
            tripData = new ArrayList<>();
        in.readTypedList(tripData, TripData.CREATOR);
        offset = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<TripPaginationState> CREATOR = new Parcelable.Creator<TripPaginationState>() {
        @Override
        public TripPaginationState createFromParcel(Parcel in) {
            return new TripPaginationState(in);
        }

        @Override
        public TripPaginationState[] newArray(int size) {
            return new TripPaginationState[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(tripData);
        dest.writeInt(offset);
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Override
    public List<?> getList() {
        return tripData;
    }

    public List<TripData> getTripData() {
        return tripData;
    }

    @Override
    public void clearAllState() {
        super.clearAllState();
        offset = 0;
    }
}
