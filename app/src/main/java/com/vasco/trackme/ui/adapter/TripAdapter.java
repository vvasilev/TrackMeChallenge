package com.vasco.trackme.ui.adapter;


import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vasco.trackme.R;
import com.vasco.trackme.model.TripData;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TripAdapter extends BaseRecyclerView<TripData, TripAdapter.TripHolder>{

    private Picasso picasso;
    private Calendar calendar;

    public TripAdapter(Activity activity, Picasso picasso, List<TripData> items) {
        super(items, activity);
        this.picasso = picasso;
        calendar = Calendar.getInstance();
    }

    @Override
    TripHolder getViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(activity).inflate(R.layout.journey_card, parent, false);
        return new TripHolder(view);
    }

    @Override
    void onBindViewHolderBase(final RecyclerView.ViewHolder viewHolder, int position) {
        final TripData item = getItem(position);
        final TripHolder holder = (TripHolder) viewHolder;
        calendar.setTimeInMillis(Long.valueOf(item.getName()));
        holder.date.setText(DateFormat.format("dd.MM.yyyy", calendar).toString());
        holder.start_time.setText(DateFormat.format("hh:mm", calendar).toString());
        holder.end_time.setText(DateFormat.format("hh:mm", calendar).toString());
    }

    @Override
    boolean checkViewHolder(RecyclerView.ViewHolder holder) {
        return holder instanceof TripHolder;
    }

    class TripHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.journey_date)
        @Nullable TextView date;

        @BindView(R.id.start_time)
        @Nullable TextView end_time;

        @BindView(R.id.end_time)
        @Nullable TextView start_time;

        TripHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (activity instanceof JourneyAdapterInterface && getAdapterPosition() >= 0)
                ((JourneyAdapterInterface)activity).onTripClick(getAdapterPosition(), items);
        }
    }

    public interface JourneyAdapterInterface{
        void onTripClick(int position, List<TripData> journeyDetails);
    }
}
