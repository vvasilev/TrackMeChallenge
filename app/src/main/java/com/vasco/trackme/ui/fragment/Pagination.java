package com.vasco.trackme.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.vasco.trackme.R;
import com.vasco.trackme.ui.utils.BasePaginationState;
import com.vasco.trackme.ui.utils.EndlessScrollListener;

import butterknife.BindView;

public abstract class Pagination extends BasePagination {

    private static final String BASE_STATE_INSTANCE = "baseStateInstance";

    @BindView(R.id.refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private EndlessScrollListener endlessScrollListener;

    protected BasePaginationState state;

    protected abstract void refreshList();
    protected abstract void onLoadItems(int page);
    protected abstract BasePaginationState createPaginationState();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (savedInstanceState != null){
            state = savedInstanceState.getParcelable(BASE_STATE_INSTANCE);
        }else {
            state = createPaginationState();
        }
        super.onViewCreated(view, savedInstanceState);
        setRecyclerView();
        swipeRefreshLayout.setOnRefreshListener(() -> {
            if (!list.isLoading())
                refreshList();
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BASE_STATE_INSTANCE, state);
    }

    @Override
    protected void setRecyclerView(){
        super.setRecyclerView();
        endlessScrollListener = new EndlessScrollListener(state) {
            @Override
            public boolean onLoadMore(int page) {
                onLoadItems(page);
                return true;
            }
        };
        list.addOnScrollListener(endlessScrollListener);
    }

    public void loadFirstPage(){
        onLoadItems(state.getCurrentPage() + 1);
        setLoading(true);
    }
}
