package com.vasco.trackme.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.squareup.picasso.Picasso;
import com.vasco.trackme.R;
import com.vasco.trackme.TheApplication;
import com.vasco.trackme.dagger.components.DaggerTripComponent;
import com.vasco.trackme.model.TripData;
import com.vasco.trackme.ui.adapter.TripAdapter;
import com.vasco.trackme.ui.utils.BasePaginationState;
import com.vasco.trackme.ui.utils.TripPaginationState;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class TripFragment extends Pagination {

    public interface TripListInterface {
        void onTripLoad(TripPaginationState tripPaginationState);
        boolean isLoadingTrip();
    }

    private TripListInterface listener;
    private TripAdapter adapter;

    @BindView(R.id.nodata_view)
    View emptyView;

    @BindView(R.id.loading_view)
    View loadingView;

    @Inject
    Picasso picasso;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TripListInterface)
            this.listener = (TripListInterface) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        super.notifyDataSetChanged();
        adapter.notifyDataSetChanged();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.journey_fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        DaggerTripComponent.builder()
                .appComponent(TheApplication.getAppComponent())
                .build().injectFragment(this);

        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState == null && state.getList().isEmpty()){
            ((TripPaginationState)state).setOffset(0);
            loadFirstPage();
        }

    }

    @Override
    protected void init(Bundle savedInstanceState) {
        if(listener.isLoadingTrip() && state.getList().isEmpty())
            list.setLoading(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void refreshList() {
        state.clearAllState();
        adapter.notifyDataSetChanged();
        loadFirstPage();
    }

    @Override
    protected void onLoadItems(int page) {
        listener.onTripLoad((TripPaginationState) state);
    }

    @Override
    protected BasePaginationState createPaginationState() {
        return new TripPaginationState();
    }

    @Override
    protected void createAdapter() {
        if (adapter == null){
            adapter = new TripAdapter(activity, picasso, (List<TripData>) state.getList());
        }
    }

    @Override
    protected RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    @Override
    protected View getEmptyView() {
        return emptyView;
    }

    @Override
    protected View getErrorView() {
        return null;
    }

    @Override
    protected View getLoadingView() {
        return loadingView;
    }

    public void refreshAdapterList(){
        super.refreshAdapter();
        adapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }

    public void addTripToList(TripData trip){
        ((TripPaginationState)state).getTripData().add(trip);
        adapter.notifyDataSetChanged();
        super.refreshAdapter();
    }

    public void removeItemFromListById(long id){
        List<TripData> trips = ((TripPaginationState) state).getTripData();
        for (int i = 0; i < trips.size(); i++){
            TripData tripData = trips.get(i);
            if (tripData.getDBid() == id){
                trips.remove(i);
                adapter.notifyItemRemoved(i);
            }
        }
    }
}
