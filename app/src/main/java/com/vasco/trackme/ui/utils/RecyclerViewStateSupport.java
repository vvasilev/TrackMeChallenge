package com.vasco.trackme.ui.utils;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

public class RecyclerViewStateSupport extends RecyclerView {

    private View emptyView;
    private View loadingView;
    private View errorView;
    private boolean isLoading;
    private boolean hasError;

    private AdapterDataObserver emptyObserver = new AdapterDataObserver() {

        @Override
        public void onChanged() {
            Adapter<?> adapter = getAdapter();
            if (adapter != null && emptyView != null) {
                if (isLoading && loadingView != null) {
                    loadingView.setVisibility(View.VISIBLE);
                    RecyclerViewStateSupport.this.setVisibility(View.GONE);
                    if (emptyView != null)
                        emptyView.setVisibility(View.GONE);
                    if (errorView != null)
                        errorView.setVisibility(GONE);
                } else if (hasError && errorView != null) {
                    errorView.setVisibility(View.VISIBLE);
                    RecyclerViewStateSupport.this.setVisibility(View.GONE);
                    if (emptyView != null)
                        emptyView.setVisibility(View.GONE);
                    if (loadingView != null)
                        loadingView.setVisibility(View.GONE);
                } else if (adapter.getItemCount() == 0) {
                    emptyView.setVisibility(View.VISIBLE);
                    RecyclerViewStateSupport.this.setVisibility(View.GONE);
                    if (loadingView != null)
                        loadingView.setVisibility(View.GONE);
                    if (errorView != null)
                        errorView.setVisibility(View.GONE);
                } else {
                    RecyclerViewStateSupport.this.setVisibility(View.VISIBLE);
                    if (emptyView != null)
                        emptyView.setVisibility(View.GONE);
                    if (loadingView != null)
                        loadingView.setVisibility(View.GONE);
                    if (errorView != null)
                        errorView.setVisibility(View.GONE);
                }
            }
        }
    };

    public RecyclerViewStateSupport(Context context) {
        super(context);
    }

    public RecyclerViewStateSupport(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RecyclerViewStateSupport(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);
        emptyObserver.onChanged();
    }

    public void setViews(View emptyView, View loadingView, View errorView) {
        this.emptyView = emptyView;
        this.loadingView = loadingView;
        this.errorView = errorView;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void notifyDataSetChanged() {
        emptyObserver.onChanged();
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
        hasError = false;
        emptyObserver.onChanged();
    }

    public void setHasError(boolean error) {
        hasError = error;
        isLoading = false;
        emptyObserver.onChanged();
    }
}
