package com.vasco.trackme.ui.utils;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.vasco.trackme.R;

public class NotificationUtils extends ContextWrapper {

    private static final String TRACKME_CHANNEL_ID = "ccom.vasco.trackme.ui.utils.NotificationUtils.TRACKME";
    private static final String TRACKME_CHANNEL_NAME = "TRACKME Channel";
    private NotificationManager mManager;

    public NotificationUtils(Context base) {
        super(base);
        createChannel();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void createChannel() {

        //create the channel
        NotificationChannel trackmeChannel = new NotificationChannel(TRACKME_CHANNEL_ID,
                TRACKME_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);

        //show notification light
        trackmeChannel.enableLights(true);

        //set notification light color
        trackmeChannel.setLightColor(Color.GREEN);

        //set whether the notification appear on the lock screen or not
        trackmeChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        getManager().createNotificationChannel(trackmeChannel);
    }

    public NotificationManager getManager() {

        if(mManager == null)
            mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        return mManager;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Notification.Builder getTrackMeNotification(String title, String description) {
        return new Notification.Builder(getApplicationContext(), TRACKME_CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(description)
                .setSmallIcon(R.drawable.ic_gps_track)
                .setAutoCancel(true);
    }
}
