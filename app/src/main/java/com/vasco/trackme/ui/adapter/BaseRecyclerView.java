package com.vasco.trackme.ui.adapter;


import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.vasco.trackme.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseRecyclerView<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    protected List<T> items;
    protected Activity activity;

    abstract VH getViewHolder(ViewGroup parent);
    abstract void onBindViewHolderBase(RecyclerView.ViewHolder holder, int position);
    abstract boolean  checkViewHolder(RecyclerView.ViewHolder holder);

    public BaseRecyclerView(final List<T> items, Activity activity) {
        this.items = items;
        this.activity = activity;
    }

    public void add(final T object) {
        items.add(object);
        notifyItemInserted(getItemCount() - 1);
    }

    public void remove(T object) {
        final int position = getPosition(object);
        items.remove(object);
        notifyItemRemoved(position);
    }

    public int getPosition(final T item) {
        return items.indexOf(item);
    }


    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM){
            return getViewHolder(parent);
        }else {
            View view = LayoutInflater.from(activity).inflate(R.layout.loading_journeydata_view, parent, false);
            return (VH) new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (checkViewHolder(holder)){
            onBindViewHolderBase(holder, position);
        }else {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOADING;
    }

    public T getItem(final int position) {
        return items.get(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.loading_journeyData)
        ProgressBar progressBar;

        LoadingViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
